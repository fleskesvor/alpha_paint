extends Node2D

onready var brush = load("res://brush.png").get_data()
onready var paint = load("res://alpha.png").get_data()

var total_pos = 0
var visited_pos = {}

func _ready():
	var bg_size = get_node("background").get_texture().get_size()
	var brush_size = brush.get_used_rect().size
	
	var units = bg_size / brush_size
	total_pos = round(units.x) * round(units.y)
	set_process_input(true)

func _input(event):
	if event.type == InputEvent.MOUSE_BUTTON:
		if event.is_pressed():
			set_process(true)
		else:
			set_process(false)

func _process(delta):
	var pos = get_viewport().get_mouse_pos()
	pos -= Vector2(brush.get_width(), brush.get_height()) / 2

	# Don't track input outside of background
	if not get_node("background").get_rect().has_point(pos):
		return

	var scale_x_pos = int(pos.x / brush.get_used_rect().size.x)
	var scale_y_pos = int(pos.y / brush.get_used_rect().size.y)

	# Add to dictionary
	visited_pos["%d:%d" % [scale_x_pos, scale_y_pos]] = true

	# Check coverage
	var percentage = 100.0 * visited_pos.size() / total_pos
	print("visited %d of %d positions (%.02f%%)" % [visited_pos.size(), total_pos, percentage])

	var canvas = get_node("light")
	var data = canvas.get_texture().get_data()
	data.brush_transfer(paint, brush, pos)
	canvas.get_texture().set_data(data)
